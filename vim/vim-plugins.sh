#!/bin/bash
#
# Copyright 2015-2024 Dimitris Zlatanidis <d.zlatanidis@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This is a simple bash script that installs vim plugins:
# supertab
# indentLine 
# vim-powerline
# vim-python-docstring
# vim-colorschemes
# vim-gitgutter
# rainbow
# jedi-vim
# auto-pairs
# NERDTree
# syntastic

# Requirements:
# vim >= 7.4 + Python or Python3

# Installation requirements as usually found 
# in all unix-like systems:
# GNU wget
# Git

ST=supertab
IL=indentLine
PL=vim-powerline
PD=vim-python-docstring
GG=vim-gitgutter
CS=colorschemes
RB=rainbow
JD=jedi-vim
AU=auto-pairs
ND=nerdtree
NG=nerdtree-git-plugin
SY=syntastic

PATHOGEN="https://tpo.pe/pathogen.vim"
VIM=".vim"
VIMRC=".vimrc"
AUTOLOAD=$(echo ${HOME}/${VIM}/autoload)
BUNDLE=$(echo ${HOME}/${VIM}/bundle)
BACKUP=$(echo ${HOME}/${VIM}/backup)
SWAP=$(echo ${HOME}/${VIM}/swap)
UNDO=$(echo ${HOME}/${VIM}/undo)

set -e

echo
echo "------------------------------------"
echo "|             S T A R T            |"
echo "------------------------------------"
echo

echo "Create directories..."
echo $VIM $AUTOLOAD $BUNDLE $BACKUP $SWAP $UNDO
mkdir -p $VIM $AUTOLOAD $BUNDLE $BACKUP $SWAP $UNDO

# copy my vimrc
cat vimrc >> $HOME/$VIMRC

# install pathogen only if new file exists
echo
echo "---------------------------"
echo "| Installing pathogen.vim |"
echo "---------------------------"
echo
wget -N --directory-prefix=$AUTOLOAD $PATHOGEN

echo
echo "-----------------------"
echo "| Installing supertab |"
echo "-----------------------"
echo
rm -rf $BUNDLE/$ST
git clone --recursive https://github.com/ervandew/supertab.git $BUNDLE/$ST

echo
echo "-------------------------"
echo "| Installing indentLine |"
echo "-------------------------"
echo
rm -rf $BUNDLE/$IL
git clone --recursive https://github.com/Yggdroot/indentLine.git $BUNDLE/$IL

echo
echo "----------------------------"
echo "| Installing vim-powerline |"
echo "----------------------------"
echo
rm -rf $BUNDLE/$PL
git clone --recursive https://github.com/Lokaltog/vim-powerline.git $BUNDLE/$PL

echo
echo "-----------------------------------"
echo "| Installing vim-python-docstring |"
echo "-----------------------------------"
echo
rm -rf $BUNDLE/$PD
git clone --recursive https://github.com/pixelneo/vim-python-docstring.git $BUNDLE/$PD

echo
echo "-------------------------------"
echo "| Installing vim-colorschemes |"
echo "-------------------------------"
echo
rm -rf $BUNDLE/$CS
git clone --recursive https://github.com/flazz/vim-colorschemes.git $BUNDLE/$CS

echo
echo "----------------------------"
echo "| Installing vim-gitgutter |"
echo "----------------------------"
echo
rm -rf $BUNDLE/$GG
git clone --recursive https://github.com/flazz/vim-colorschemes.git $BUNDLE/$GG

echo
echo "----------------------"
echo "| Installing rainbow |"
echo "----------------------"
echo
rm -rf $BUNDLE/$RB
git clone --recursive https://github.com/flazz/vim-colorschemes.git $BUNDLE/$RB

echo
echo "-----------------------"
echo "| Installing jedi-vim |"
echo "-----------------------"
echo
rm -rf $BUNDLE/$JD
git clone --recursive https://github.com/davidhalter/jedi-vim.git $BUNDLE/$JD

echo
echo "-------------------------"
echo "| Installing auto-pairs |"
echo "-------------------------"
echo
rm -rf $BUNDLE/$AU
git clone --recursive https://github.com/jiangmiao/auto-pairs.git $BUNDLE/$AU

echo
echo "-----------------------"
echo "| Installing NERDTree |"
echo "-----------------------"
echo
rm -rf $BUNDLE/$ND
git clone --recursive https://github.com/preservim/nerdtree.git $BUNDLE/$ND

echo
echo "----------------------------------"
echo "| Installing nerdtree-git-plugin |"
echo "----------------------------------"
echo
rm -rf $BUNDLE/$NG
git clone --recursive https://github.com/Xuyuanp/nerdtree-git-plugin.git $BUNDLE/$NG

echo
echo "------------------------"
echo "| Installing syntastic |"
echo "------------------------"
echo
rm -rf $BUNDLE/$SY
git clone --recursive https://github.com/vim-syntastic/syntastic.git $BUNDLE/$SY

echo
echo
echo "Done!"
