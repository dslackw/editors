This is a simple bash script that installs vim plugins and a `.vimrc` file to make a
powerful Python IDE.

- vim-pathogen: https://github.com/tpope/vim-pathogen
- indentLine: https://github.com/Yggdroot/indentLine
- supertab: https://github.com/ervandew/supertab
- vim-powerline: https://github.com/Lokaltog/vim-powerline
- vim-colorschemes: https://github.com/flazz/vim-colorschemes
- vim-python-docstring: https://github.com/pixelneo/vim-python-docstring
- vim-gitgutter: https://github.com/airblade/vim-gitgutter
- rainbow: https://github.com/luochen1990/rainbow
- jedi-vim: https://github.com/davidhalter/jedi-vim
- auto-pairs: https://github.com/jiangmiao/auto-pairs
- NERDTree: https://github.com/preservim/nerdtree
- nerdtree-git-plugin: https://github.com/Xuyuanp/nerdtree-git-plugin

Requirements:

.. role:: raw-html(raw)
    :format: html

| vim & gvim >= 9.0 + Python3
| flak8
| pylint
| JetBrains fonts
| 

Installation requirements as usually found in all Unix-like systems:

- GNU wget
- Git


NOTE: Backup your `.vimrc` file before proceeding to installation.

Installation:

.. code-block:: bash
    
    $ python3 -m ~/py_venv


Screenshot:

.. image:: https://gitlab.com/dslackw/vim-plugins/raw/master/vim-plugins-ss.png
    :target: https://gitlab.com/dslackw/vim-plugins
