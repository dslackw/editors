;;; init.el --- User initialization file.
;;; Commentary:
;; Emacs configuration for python IDE.
;; Dependencies:
;; M-x package-install RET python-insert-docstring
;; M-x package-install RET minimap
;; M-x package-install RET undo-tree
;; M-x package-install RET centaur-tabs
;; python3 -m venv ~/py_venv
;; source ~/py_venv/bin/activate.fish
;; pip3 install pylint flake8_annotations flake8-docstrings python-lsp-server[all]
;; pandoc is used for markdown live preview.

;; Thanks System Crafters for some configs.
;; https://github.com/daviwil/emacs-from-scratch/
;; https://www.youtube.com/live/jPXIP46BnNA

;; Emacs has a built in package manager but it doesn’t make it easy
;; to automatically install packages on a new system the first time
;; you pull down your configuration, use-package is a really helpful
;; package used in this configuration to make it a lot easier to
;; automate the installation and configuration of everything else we use.

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)
(setq use-package-always-ensure t)

;; Use dashboard package
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner 'official) ; Use Emacs logo
  (setq dashboard-items '((recents  . 5)    ; Show recent files
                          (bookmarks . 5)  ; Show bookmarks
                          (projects . 5))) ; Show projects
  )

(setq inhibit-startup-screen t)

;; Close *Messages* and *scratch* buffers on startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (when (get-buffer "*Messages*")
              (kill-buffer "*Messages*"))
            (when (get-buffer "*scratch*")
              (kill-buffer "*scratch*"))))

;; This section configures basic UI settings that remove unneeded elements
;; to make Emacs look a lot more minimal and modern. If you’re just getting
;; started in Emacs, the menu bar might be helpful so you can remove the
;; (menu-bar-mode -1) line if you’d like to still see that.
(setq inhibit-startup-message t)
(setq make-backup-files nil) ; stop creating ~ files

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

;; (menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell t)

;; Quickly move the cursor to the beginning or end of the buffer.
(global-set-key (kbd "C-c b") 'beginning-of-buffer)
(global-set-key (kbd "C-c e") 'end-of-buffer)

;; Select and move a block of code left or right.
(global-set-key (kbd "C-c <right>") (lambda () (interactive) (indent-rigidly (region-beginning) (region-end) 4)))
(global-set-key (kbd "C-c <left>") (lambda () (interactive) (indent-rigidly (region-beginning) (region-end) -4)))

;; Remapping ESC to keyboard-escape-quit.
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(column-number-mode)
(global-display-line-numbers-mode t)

;; Set the default font family and size
(set-face-attribute 'default nil :family "JetBrains Mono NL" :height 110)

;; Disable line numbers for some modes
 (dolist (mode '(term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook
		vterm-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; doom-themes is a great set of themes with a lot of variety and support
;; for many different Emacs modes. Taking a look at the screenshots might
;; help you decide which one you like best. You can also run M-x
;; counsel-load-theme to choose between them easily.
(use-package doom-themes
  :init (load-theme 'doom-material-dark t))

;; doom-modeline is a very attractive and rich (yet still minimal) mode
;; line configuration for Emacs. The default configuration is quite good
;; but you can check out the configuration options for more things you
;; can enable or disable.
;; NOTE: The first time you load your configuration on a new machine,
;; you’ll need to run `M-x all-the-icons-install-fonts` or
;; `M-x nerd-icons-install-fonts` so that mode line icons display correctly.
(use-package all-the-icons)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; Ivy is an excellent completion framework for Emacs. It provides
;; a minimal yet powerful selection menu that appears when you open
;; files, switch buffers, and for many other tasks in Emacs. Counsel
;; is a customized set of commands to replace `find-file` with
;; `counsel-find-file`, etc which provide useful commands for each
;; of the default completion commands.
;; ivy-rich adds extra columns to a few of the Counsel commands to
;; provide more information about each item.
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

;; Helpful adds a lot of very helpful (get it?) information to Emacs’
;; describe- command buffers. For example, if you use describe-function,
;; you will not only get the documentation about the function, you will
;; also see the source code of the function and where it gets used in
;; other places in the Emacs configuration. It is very useful for
;; figuring out how things work in Emacs.
(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; lsp-ui is a set of UI enhancements built on top of lsp-mode
;; which make Emacs feel even more like an IDE. Check out the
;; screenshots on the lsp-ui homepage (linked at the beginning
;; of this paragraph) to see examples of what it can do.
(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package lsp-treemacs
  :after lsp)

(lsp-treemacs-sync-mode 1)
;; The built-in ‘python-mode’ supports, from its help, “Syntax highlighting, Indentation,
;; Movement, Shell interaction, Shell completion, Shell virtualenv support, Shell package
;; support, Shell syntax highlighting, Pdb tracking, Symbol completion, Skeletons, FFAP,
;; Code Check, ElDoc, Imenu.
(use-package python-mode
  :ensure t
  :hook (python-mode . lsp-deferred)
  :custom
  (python-shell-interpreter "/home/dslackw/py_venv/bin/python3")
  (dap-python-executable "/home/dslackw/py_venv/bin/python3")
  (flycheck-python-pylint-executable "/home/dslackw/py_venv/bin/pylint")
  ;; Customize the pylint arguments to include the error ID
  (setq flycheck-python-pylint-args
        '("--msg-template='{line}:{column}:{category}:{msg_id}:{msg} ({symbol})'"))
  (flycheck-python-flake8-executable "/home/dslackw/py_venv/bin/flake8")
  (dap-python-debugger 'debugpy)
  :config
  (require 'dap-python))

(defvar flycheck-flake8 nil)
(defvar flycheck-pylint nil)

(setq flycheck-flake8 "/home/dslackw/.flake8")
(setq flycheck-pylint "/home/dslackw/.pylintrc")

;; Flycheck is a modern on-the-fly syntax checking extension for GNU Emacs.
(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))

(add-hook 'prog-mode-hook 'flyspell-prog-mode)
(setq ispell-dictionary "english")  ; Set default dictionary to English
(global-set-key (kbd "C-;") 'flyspell-auto-correct-word)

;; Set keybinding for auto insert docstring.
(defun set-python-keybindings ()
  "Set up keybindings for Python mode.

  This function binds the key combination C & c i to the function
  python-insert-docstring-with-google-style-at-point.  In simpler terms,
  pressing C & c i while in Python mode will insert a Google style docstring
  at the current cursor location."
  (local-set-key (kbd "C-c i") 'python-insert-docstring-with-google-style-at-point)
  )

(add-hook 'python-mode-hook 'set-python-keybindings)

;; Company Mode provides a nicer in-buffer completion interface than completion-at-point
;; which is more reminiscent of what you would expect from an IDE. We add a simple
;; configuration to make the keybindings a little more useful (TAB now completes the
;; selection and initiates completion at the current location if needed).
;; We also use company-box to further enhance the look of the completions with icons and
;; better overall presentation.
(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(add-hook 'after-init-hook 'global-company-mode)

;; Client for Language Server Protocol (v3.14). lsp-mode aims to provide
;; IDE-like experience by providing optional integration with the most
;; popular Emacs packages like company, flycheck and projectile.
(defvar lsp-pylsp-server-command nil)
(defvar lsp-python-ms-python-executable nil)

(use-package lsp-mode
  :ensure t
  :hook (python-mode . lsp)
  :config
  (setq lsp-pylsp-server-command '("/home/dslackw/py_venv/bin/pylsp"))
  (setq lsp-python-ms-python-executable "/home/dslackw/py_venv/bin/python3"))

;; Enable pylint plugin
;; (setq lsp-pylsp-plugins-pylint-enabled t)

;; To turn off lsp-mode's flycheck backend:
(defvar lsp-diagnostics-provider nil)

(setq lsp-diagnostics-provider :none)
(setq lsp-enable-symbol-highlighting nil)

;; rainbow-delimiters is useful in programming modes because it colorizes
;; nested parentheses and brackets according to their nesting depth. This
;; makes it a lot easier to visually match parentheses in Emacs Lisp code
;; without having to count them yourself.
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Magit is a complete text-based user interface to Git.
(unless (package-installed-p 'magit)
  (package-refresh-contents)
  (package-install 'magit))

;; diff-hl-mode highlights uncommitted changes on the left side of the window.
(unless (package-installed-p 'diff-hl)
  (package-refresh-contents)
  (package-install 'diff-hl))

(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

(global-diff-hl-mode)

(diff-hl-flydiff-mode)

;; vterm is as good as terminal emulation gets in Emacs (at the time of writing)
;; and the most performant, as it is implemented in C.
(unless (package-installed-p 'vterm)
  (package-refresh-contents)
  (package-install 'vterm))

(defvar vterm-shell nil)
(setq vterm-shell "/usr/bin/fish")  ; Adjust the path to Fish if necessary

(global-set-key (kbd "C-c v") 'vterm-other-window)

;; Redefine C-k to delete the whole line whenever cursor is.
(global-set-key (kbd "C-k") 'kill-whole-line)

;; Auto-save settings
(setq auto-save-timeout 10)  ; Auto-save every 10 seconds of idle time
;; (setq auto-save-default t)  ; Enable auto-saving for all buffers

(setq default-directory "/home/dslackw/Gitlab")

;; Install and configure projectile
(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  ;; Recommended keymap prefix on Windows/Linux
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

;; Install and configure treemacs-projectile
(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)


;; markdown-mode is a major mode for editing Markdown-formatted text.
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

;; Install and configure ace-window
(use-package ace-window
  :ensure t
  :bind ("M-o" . ace-window)
  :config
  (setq aw-dispatch-always t))

;; Enable windmove default keybindings
(windmove-default-keybindings)

;; Install and configure treemacs
(use-package treemacs
  :ensure t
  :bind
  (:map global-map
        ([f8] . treemacs))
  :config
  (setq treemacs-is-never-other-window t)
  (with-eval-after-load 'treemacs
    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-git-mode 'deferred)))

;; (setq treemacs-show-hidden-files t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

;; Enable M-0 to select treemacs window.
(global-set-key (kbd "M-<left>") 'treemacs-select-window)

;; Load the minimap package
(require 'minimap)

;; Set minimap to the right side
(setq minimap-window-location 'right)

;; Optional: Customize minimap settings
(setq minimap-width-fraction 0.1) ; Adjust the width of the minimap
(setq minimap-update-delay 0.2)   ; Adjust the update delay

;; Enable minimap globally (optional)
;; (minimap-mode 1)

;; Keybinding to toggle minimap
(global-set-key (kbd "C-c m") 'minimap-mode)
;; Set cursor into a line
;; (setq-default cursor-type 'bar)

;; Deleting Trailing Whitespace Automatically on Save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Show vertical tab lines (also known as indentation guides).
(use-package highlight-indent-guides
  :ensure t
  :hook ((prog-mode yaml-mode) . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'character)
  ;; (setq highlight-indent-guides-character ?\|)
  (setq highlight-indent-guides-auto-enabled nil)
  (setq highlight-indent-guides-responsive 'top)
  (set-face-foreground 'highlight-indent-guides-character-face "gray20") ; Set the color of the indent guides
  (set-face-foreground 'highlight-indent-guides-top-character-face "dimgray"))

;; Save last position in files
(save-place-mode 1)
(setq save-place-file "/home/dslackw/.emacs.d/places")

;; Save command histories
(savehist-mode 1)
(setq savehist-file "/home/dslackw/.emacs.d/savehist")
(setq savehist-additional-variables
      '(kill-ring
        search-ring
        regexp-search-ring))

;; Enable and configure undo-tree
(require 'undo-tree)
(global-undo-tree-mode)
(setq undo-tree-auto-save-history t)
(setq undo-tree-history-directory-alist
      `(("." . ,(expand-file-name "undo-tree-history" user-emacs-directory))))

;; Show buffers like tabs, providing a more modern and intuitive
;; way to navigate between open files and buffers.

(use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward))

(centaur-tabs-change-fonts "JetBrains Mono NL" 109)

;; Basic customization
(setq centaur-tabs-set-icons t)
(setq centaur-tabs-set-modified-marker t)
(setq centaur-tabs-modified-marker "*")
(setq centaur-tabs-style "wave")
(setq centaur-tabs-height 32)
(setq centaur-tabs-set-bar 'over)
(setq centaur-tabs-set-close-button t)
(setq centaur-tabs-close-button "x")
(setq centaur-tabs-enable-buffer-reordering t)
(setq centaur-tabs-cycle-scope 'tabs)
(setq centaur-tabs-gray-out-icons 'buffer)
(setq centaur-tabs-set-bar 'over)

;; Optional: Grouping buffers by projects or other criteria
;; (setq centaur-tabs-adjust-buffer-order-function 'centaur-tabs-adjust-buffer-order)

;; Directory to store backups and autosaves
(setq backup-directory-alist `((".*" . "~/.emacs.d/backups/")))
(setq auto-save-file-name-transforms `((".*" "~/.emacs.d/backups/" t)))

;; Keep more backup versions
(setq version-control t)     ;; Use version numbers for backups
(setq kept-new-versions 20)  ;; Number of newest versions to keep
(setq kept-old-versions 5)   ;; Number of oldest versions to keep
(setq delete-old-versions t) ;; Automatically delete excess backups
(setq backup-by-copying t)   ;; Copy files instead of moving

;; Autosave settings
(setq auto-save-default t)   ;; Enable auto-save
(setq auto-save-timeout 20)  ;; Number of seconds idle time before auto-save (default: 30)
(setq auto-save-interval 200) ;; Number of keystrokes between auto-saves (default: 300)

;; Remembering minibuffer prompt history
(setq history-length 25)
(savehist-mode 1)

;; Automatically revert buffers for changed files outside of Emacs.
(global-auto-revert-mode 1)

;; Revert Dired and other buffers.
(setq global-auto-revert-non-file-buffers t)

;; ============================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(centaur-tabs magit virtualenv lsp-jedi jedi pyenv-mode python-insert-docstring corfu python-mode dap-mode lsp-mode doom-themes)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'upcase-region 'disabled nil)

(provide 'init)
;;; init.el ends here
