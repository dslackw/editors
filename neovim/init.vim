call plug#begin()

" List your plugins here
Plug 'junegunn/vim-plug'
Plug 'reewr/vim-monokai-phoenix'
Plug 'tpope/vim-sensible'
Plug 'vim-syntastic/syntastic'
Plug 'Yggdroot/indentLine'
Plug 'freddiehaddad/feline.nvim'
Plug 'jiangmiao/auto-pairs'
Plug 'davidhalter/jedi-vim'
Plug 'pixelneo/vim-python-docstring'
Plug 'ervandew/supertab'
Plug 'airblade/vim-gitgutter'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'luochen1990/rainbow'
Plug 'lewis6991/gitsigns.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'ryanoasis/nerd-fonts'
Plug 'tpope/vim-commentary'

call plug#end()

lua require('feline').setup()
lua require('gitsigns').setup()

colorscheme monokai-phoenix

" Enable folding
set foldmethod=indent
set foldlevel=99
set undofile " Enable persistent undo
set undodir=~/.config/nvim/undodir " Set the directory where undo files will be stored
syntax on " syntax highlighting
set cc=80 " set an 80 column border for good coding style
set ignorecase " case insensitive.
set clipboard+=unnamedplus
set number
set smartindent
set autoindent " indent a new line the same amount as the line just typed.
set tabstop=4 " number of columns occupied by a tab. 
set cursorline " highlight current cursorline.
set laststatus=2 " Always show the statusline.
set t_Co=256 " Explicitly tell Vim that the terminal supports 256 colors.
set noswapfile " disable creating swap file.
set backupdir=~/.cache/vim " Directory to store backup files.
set showmatch " show matching
set updatetime=250

" Ps = 0  -> blinking block.
" Ps = 1  -> blinking block (default).
" Ps = 2  -> steady block.
" Ps = 3  -> blinking underline.
" Ps = 4  -> steady underline.
" Ps = 5  -> blinking bar (xterm).
" Ps = 6  -> steady bar (xterm).
let &t_SI = "\e[5 q"
let &t_EI = "\e[1 q"

let g:python3_host_prog = '/home/dslackw/py_venv/bin/python3'

let g:indentLine_setColors = 239
let g:indentLine_char = '│'
let g:rainbow_active = 1

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
let g:syntastic_aggregate_errors = 1

let g:syntastic_python_interpreter = '~/py_venv/bin/python3'
let g:syntastic_python_checkers = ['pylint', 'flake8']
let g:syntastic_python_flake8_args='--ignore=E501,ANN101,ANN204,D100,D107'
" Several people have expressed interest in showing the old message IDs
" along with the error messages. You can override the message format in
" 'g:syntastic_python_pylint_post_args'. Please note that the new format must
" start with \"{path}:{line}:{column}:{C}: \", otherwise syntastic will not
" recognise any messages. Example: >
let g:syntastic_python_pylint_post_args = '--msg-template="{path}:{line}:{column}:{C}: [{symbol} {msg_id}] {msg}"'
" Remove all trailing whitespace by pressing F5"


:nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" Enable folding with the spacebar
:nnoremap <space> za

" Set F6 to autocomplete docstring
:nnoremap <F6> :Docstring

" Save and restore cursor position
if has("autocmd")
  au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
endif
